import pandas as pd
import numpy as np
import json
import string
import re
from gensim.models import KeyedVectors 
from tqdm import tqdm
from sklearn.cluster import KMeans
from pymystem3 import Mystem
from sklearn.neighbors import KDTree


m = Mystem()

with open('stopwords-ru.txt', 'r', encoding='utf-8', errors='ignore') as f:
    stop_words = f.readlines()
    stop_words = [x.strip() for x in stop_words]


all_data = []

translator = str.maketrans('', '', string.punctuation)

REVIEWS_COUNT = 1000

with open('all_reviews_texts_lemm.txt', 'r', encoding='utf-8', errors='ignore') as f:
    f = [next(f) for x in tqdm(range(REVIEWS_COUNT))]

    for review in tqdm(f):
        try:
            json_string = json.loads(review)
            cat2 = json_string['cat2']

            if cat2 == 'Лекарственные средства':
                description = json_string['lemm_text'].translate(translator).lower()

                lemmas_list = [x for x in description.split(' ') if x not in stop_words]

                lemmas = ' '.join(lemmas_list)
                lemmas = re.sub(' +', ' ', lemmas)

                if lemmas != '':
                    all_data.extend(lemmas.strip().split(' '))
        except:
            continue


def clustering_on_wordvecs(word_vectors, num_clusters):
    kmeans_clustering = KMeans(n_clusters=num_clusters, init='k-means++')
    idx = kmeans_clustering.fit_predict(word_vectors)
    
    return kmeans_clustering.cluster_centers_, idx


filename = 'araneum_none_fasttextcbow_300_5_2018.model'

model = KeyedVectors.load(filename)

Z = model.wv.syn0


centers, clusters = clustering_on_wordvecs(Z, 30)
centroid_map = dict(zip(model.wv.index2word, clusters))


def get_top_words(index2word, k, centers, wordvecs):
    tree = KDTree(wordvecs)
    closest_points = [tree.query(np.reshape(x, (1, -1)), k=k) for x in centers]
    closest_words_idxs = [x[1] for x in closest_points]
    closest_words = {}
    
    for i in range(0, len(closest_words_idxs)):
        closest_words['Cluster #' + str(i)] = [index2word[j] for j in closest_words_idxs[i][0] if index2word[j] in all_data]
    
    print(closest_words)

	
    df = pd.DataFrame(closest_words)
    df.index = df.index + 1
    
    return df

	
top_words = get_top_words(model.wv.index2word, 50, centers, Z)


with open('./clusters/clusters_araneum_filtered.txt', 'w', encoding='utf-8', errors='ignore') as f:
    for cluster in top_words:
        f.write(cluster + ' ')
        
        for word in top_words[cluster]:
            f.write(word + ' ')
            
        f.write('\n')